﻿using System;

//Our Namespace
namespace NumberGuesser
{
    
    //Main Class
    class Program
    {
        
        //Entry Point
        static void Main(string[] args)
        {
            getappinfo();
            

            //Game Variables
            string playerName;
            int guesses = 0;
            int guess = 0;
            Random random = new Random();
            int correctNumber = random.Next(1,100);
           

            //Ask User Name
            Console.WriteLine("What is your name: ");
            playerName = Console.ReadLine();
            Console.WriteLine("Hello {0}, lets play a game...", playerName);
            Console.WriteLine("Pick a number from 1 to 100, you have 7 tries: ");
            while(guess != correctNumber && guesses < 7 ){
                string input = Console.ReadLine();
                if(!int.TryParse(input,out guess)){
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Bruh, that wasn't even a number:");
                    Console.ResetColor();
                    continue;
                }
                //Cast to int and put in guess
                guess = Int32.Parse(input);
               
                if (guess == correctNumber){
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Congratulations! You picked the correct number!");
                    Console.WriteLine("Would you like to play again? [Y | N]");
                    string answer = Console.ReadLine().ToUpper();
                    if(answer == "Y"){
                        guess = 0;
                        guesses = 0;
                        Console.Clear();
                        Console.WriteLine("Pick a number from 1 to 100, you have 7 tries: ");
                        correctNumber = random.Next(1, 100);
                    }
                    Console.ResetColor();

                    break;
                }


                Console.ForegroundColor = ConsoleColor.Red;
                if(guess < correctNumber){
                    Console.WriteLine("Incorrect: correct number is higher");
                    guesses++;
                    if(guesses >= 7)
                    {
                        Console.WriteLine("Sorry you lost, Would you like to play again? [Y | N]");
                        string answer = Console.ReadLine().ToUpper();
                        if (answer == "Y")
                        {
                            guess = 0;
                            guesses = 0;
                            Console.Clear();
                            Console.WriteLine("Pick a number from 1 to 100, you have 7 tries: ");
                            correctNumber = random.Next(1, 100);
                        }
                    }
                }
                else{
                    Console.WriteLine("Incorrect: correct Number is lower");
                    guesses++;
                    if (guesses >= 7)
                    {
                        Console.WriteLine("Sorry you lost, Would you like to play again? [Y | N]");
                        string answer = Console.ReadLine().ToUpper();
                        if (answer == "Y")
                        {
                            guess = 0;
                            guesses = 0;
                            Console.Clear();
                            Console.WriteLine("Pick a number from 1 to 100, you have 7 tries: ");
                            correctNumber = random.Next(1, 100);
                        }
                    }
                }
                if(guesses == 6){
                    Console.WriteLine("You have one more shot, make it count:");
                }
                Console.ResetColor();
            }




        }

        static void  getappinfo()
        {
            string appName = "Number Guesser";
            string appVersion = "1.0.0";
            string appAuthor = "Ethan Jones";

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("{0}: Version {1} by {2}", appName, appVersion, appAuthor);
            Console.ResetColor();


        }
    }
}
